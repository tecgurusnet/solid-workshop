class UseCase{
    constructor(){
        this.externalService = new ExternalService();
    }
    
    doSomething(){
         this.externalService.doExternalTask();
    }
}
    
class ExternalService{
     doExternalTask(){
         console.log("Doing task...")
     }
}

new UseCase().doSomething();