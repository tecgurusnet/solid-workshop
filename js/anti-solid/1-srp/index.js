class UseCase{
    doSomethingWithTaxes(){
      console.log("Hacer una accion con los impuestos ...")
    }
  
    saveChangesInDatabase(){
      console.log("Guardando en base de datos...")
    }
  
    sendEmail(){
      console.log("Enviando email ...")
    }
  }
  
  function start(){
    const myUseCase = new UseCase()
  
    myUseCase.doSomethingWithTaxes();
    myUseCase.saveChangesInDatabase();
    myUseCase.sendEmail();
  }
  
  start();