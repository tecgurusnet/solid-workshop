var axios = require('axios');

class TodoService{

  requestTodoItems(callback){
    const url = 'https://jsonplaceholder.typicode.com/todos/';
		
    axios
      .get(url)
      .then(callback)
  }
}

new TodoService().requestTodoItems(response => console.log(response.data))