package net.tecgurus.workshop.ocp;

public class Discount {

	public double apply(double price) {
		return price * 0.5;
	}
}
