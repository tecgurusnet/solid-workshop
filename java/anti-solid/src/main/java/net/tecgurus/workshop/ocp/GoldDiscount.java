package net.tecgurus.workshop.ocp;

public class GoldDiscount {

	public double apply(double price) {
		return price * 0.2;
	}
}
