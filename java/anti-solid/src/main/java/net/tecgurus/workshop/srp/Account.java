package net.tecgurus.workshop.srp;

public class Account {
	private int id;
	private String number;
	private double balance;
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public double getBalance() {
		return balance;
	}

	public void setBalance(double balance) {
		this.balance = balance;
	}

	public double computeTaxes() {
		return 0.0;
	}
	
	public void showDetails() {
		System.out.println("Account .......");
	}
	
	public void saveAccount(Account account) { 
		//Saving account ...
	}
	
	public void updateAccount(Account account) {
		//Updating account ...
	}
	
}
