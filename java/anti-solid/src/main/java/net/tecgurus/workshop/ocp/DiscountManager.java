package net.tecgurus.workshop.ocp;

public class DiscountManager {

	double apply(double price, Discount discount) {
		return discount.apply(price);
	}
	
	double apply(double price, GoldDiscount discount) {
		return discount.apply(price);
	}
	
	double apply(double price, BlackFridayDiscount discount) {
		return discount.apply(price);
	}
	
	//Christmas, etc................more and more discounts.
}
