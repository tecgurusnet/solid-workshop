package net.tecgurus.workshop.lsp;

import java.util.ArrayList;
import java.util.List;

public class Main {

	public static void main(String[] args) {
		List<ComissionProcess> processes = new ArrayList<>();
		processes.add(new HealthComissionProcess());
		processes.add(new LifeComissionProcess());
		processes.add(new TravelComissionProcess());
		for (ComissionProcess comissionProcess : processes) {
			comissionProcess.compute();
		}
	}

}
