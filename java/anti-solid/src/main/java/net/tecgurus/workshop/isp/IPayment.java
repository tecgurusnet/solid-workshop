package net.tecgurus.workshop.isp;

public interface IPayment {
	void calculatePayment();
	void creditCardPayment();
	//Nos piden agregar transferencia bancaria...
	void bankTransferPayment();
	//Si al final agregamos pago en efectivo...
	void cashPayment();
}
