package net.tecgurus.workshop.dip;

public class UserService {

	public User findUser() {
		UserRepository userRepository = new UserRepository();
		User user = userRepository.findUser();
		return user;
	}
}
