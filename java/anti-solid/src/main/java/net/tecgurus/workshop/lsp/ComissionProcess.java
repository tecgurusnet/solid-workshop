package net.tecgurus.workshop.lsp;

import java.util.Date;
import java.util.Random;

public class ComissionProcess {

	private int idProcess = new Random().nextInt();
	private Date executionDate = new Date();
	
	public void compute() {
		System.out.println("Computing comission for agents: pid " + this.idProcess);
	}

	public int getIdProcess() {
		return idProcess;
	}

	public void setIdProcess(int idProcess) {
		this.idProcess = idProcess;
	}

	public Date getExecutionDate() {
		return executionDate;
	}

	public void setExecutionDate(Date executionDate) {
		this.executionDate = executionDate;
	}
	
	
	
}
