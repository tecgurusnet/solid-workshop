package net.tecgurus.workshop.ocp;

public class BlackFridayDiscount {

	public double apply(double price) {
		return price * 0.4;
	}
}
