package net.tecgurus.workshop.dip;

public class User {
	private int idUser;
	private String email;
	private String name;
	private String lastname;
	
	public User(int idUser, String email, String name, String lastname) {
		this.idUser = idUser;
		this.email = email;
		this.name = name;
		this.lastname = lastname;
	}
	public int getIdUser() {
		return idUser;
	}
	public void setIdUser(int idUser) {
		this.idUser = idUser;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getLastname() {
		return lastname;
	}
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	
	
}
